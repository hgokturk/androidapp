package com.example.deathstar.ankettest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class Customlistadapter extends ArrayAdapter<String> {
    ArrayList<String> color_names;
    ArrayList<String> images;

    /*
     * image_id de color_names gibi değiştirilerek
     * arraylist yapılacak
     */
    //Integer[] image_id;
    Context context;

    public Customlistadapter(Activity context, ArrayList<String> img, ArrayList<String> text) {
        super(context, R.layout.row_one_cikanlar, text);
// TODO Auto-generated constructor stub
        this.color_names = text;
        this.images = img;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View single_row = inflater.inflate(R.layout.row_one_cikanlar, null,
                true);

        single_row.setBackgroundColor(Color.parseColor("#D8EAF0"));

        TextView textView = (TextView) single_row.findViewById(R.id.textView);
        ImageView imageView = (ImageView) single_row.findViewById(R.id.imageView);

        //textView.setBackgroundColor(Color.parseColor("#C5DCE3"));
        //imageView.setBackgroundColor(Color.parseColor("#54AECC"));




        Uri catURI;
        //catURI = Uri.parse("http://i.telegraph.co.uk/multimedia/archive/02357/eso-summary_2357457k.jpg");

        catURI = Uri.parse(images.get(position).toString());
        ImageLoader imgGet = new ImageLoader(catURI, imageView, 280, 240);
        imgGet.execute();

        textView.setText(color_names.get(position));
        //imageView.setImageResource(image_id[position]);
        return single_row;
    }
}
