package com.example.deathstar.ankettest;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Anket extends ActionBarActivity implements ActionBar.TabListener {

    private final String LOGGEDIN = "isLoggedin";
    private final String LOGIN = "login";

    private static Context context;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    public void checkLogin(){
        //@todo:checklogin() in onStart() ta çalıştırılmasına yönelik tavsiyeler var ya da onAttachToWindow() a
        SharedPreferences pref = getApplicationContext().getSharedPreferences(LOGIN, Context.MODE_PRIVATE); // 0 - for private mode
        //Editor editor = pref.edit();

        //editor.clear().commit();

        boolean isLoggedIn = pref.getBoolean(LOGGEDIN, false);

        if(isLoggedIn == false){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

            overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.checkLogin();
        Log.d("Lifecycle: ", "onStart");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anket);

        context = getApplicationContext();


        /*
         * telefon id sini (uniqe) db'de saklamak adına al
         */
        /*TelephonyManager tManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String uid = tManager.getDeviceId(); // imei number
        String imsistring = tManager.getSubscriberId(); //IMSI for a GSM phone
        */

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_anket, menu);
        return true;
    }

    public static Context getContext(){
        return context;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.cikis:
                SharedPreferences pref = getApplicationContext().getSharedPreferences(LOGIN, Context.MODE_PRIVATE); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();

                editor.clear().commit();

                Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intent);

                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


        /*if (id == R.id.action_settings) {
            return true;
        }*/
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 0:
                    return kategoriler.newInstance(position + 1);
                case 1:
                    return oneCikanlar.newInstance(position + 1);
                case 2:
                    return anketlerim.newInstance(position + 1);
            }
            return null;
            //return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class kategoriler extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static kategoriler newInstance(int sectionNumber) {
            kategoriler fragment = new kategoriler();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public kategoriler() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.kategoriler, container, false);
            return rootView;
        }
    }

    public static class oneCikanlar extends Fragment {

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        //String color_names[] = {"red", "green", "blue", "yellow"};
        //Integer image_id[] = {R.drawable.red, R.drawable.green, R.drawable.blue/*, R.drawable.yellow*/};
        ArrayList<String> color_names = new ArrayList<String>();
        ArrayList<String> img = new ArrayList<>();

        //Integer image_id[];

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static oneCikanlar newInstance(int sectionNumber) {
            oneCikanlar fragment = new oneCikanlar();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public oneCikanlar() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            JSONArray test = null;
            View rootView = inflater.inflate(R.layout.one_cikanlar, container, false);

            trending ttt = new trending();
            try {
                test = ttt.execute().get();

                for(int i = 0; i < test.length(); i++){
                    JSONObject obj = null;
                    try {
                        obj = test.getJSONObject(i);
                        color_names.add(obj.getString("baslik"));
                        img.add(obj.getString("img"));
                    } catch (JSONException e) {
                        Log.d("hata: " , e.getMessage());
                        e.printStackTrace();
                    }

                }


                //Toast.makeText(getActivity(),test.toString(), Toast.LENGTH_LONG).show();
            } catch (InterruptedException e) {
                Log.d("hata: " , e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("hata: " , e.getMessage());
                e.printStackTrace();
            }


            Customlistadapter adapter = new Customlistadapter(getActivity() ,img, color_names);
            ListView lv = (ListView) rootView.findViewById(R.id.listView);
            lv.setAdapter(adapter);
            final JSONArray finalTest = test;
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    JSONObject Data = null;
                    String baslik = null;
                    String secim1 = null;
                    String secim2 = null;
                    String secim3 = null;
                    String secim4 = null;
                    try {
                        Data = finalTest.getJSONObject(position);

                        secim1 = Data.getString("secim1");
                        secim2 = Data.getString("secim2");
                        secim3 = Data.getString("secim3");
                        secim4 = Data.getString("secim4");
                        baslik = Data.getString("baslik");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    //dialog
                    final CharSequence[] secimler = new CharSequence[4];
                    secimler[0] = secim1;
                    secimler[1] = secim2;
                    secimler[2] = secim3;
                    secimler[3] = secim4;

                    AlertDialog.Builder alt_bld = new AlertDialog.Builder(getActivity());
                    alt_bld.setIcon(R.drawable.ic_launcher); // @todo:image view dan image koy
                    alt_bld.setTitle(baslik);
                    alt_bld.setSingleChoiceItems(secimler, -1, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            Toast.makeText(getActivity(),
                                    "Phone Model = " + secimler[item], Toast.LENGTH_SHORT).show();
                        }
                    });
                    AlertDialog alert = alt_bld.create();
                    alert.show();
                    //dialog
                }
            });

            return rootView;
        }
    }

    public static class anketlerim extends Fragment {

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static anketlerim newInstance(int sectionNumber) {
            anketlerim fragment = new anketlerim();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public anketlerim() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.anketlerim, container, false);
            return rootView;
        }
    }

}