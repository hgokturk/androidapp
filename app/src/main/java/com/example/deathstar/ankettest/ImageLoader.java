package com.example.deathstar.ankettest;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ImageLoader extends AsyncTask<Uri, Integer, Bitmap> {
    private Uri imageUri;

    private ImageView imageView;

    private int preferredWidth = 200;
    private int preferredHeight = 160;

    public ImageLoader( Uri uri, ImageView imageView, int scaleWidth, int scaleHeight ) {
        this.imageUri = uri;
        this.imageView = imageView;
        this.preferredWidth = scaleWidth;
        this.preferredHeight = scaleHeight;
    }

    public Bitmap doInBackground(Uri... params) {
        if( imageUri == null ) return null;
        String url = imageUri.toString();

        if(fileExistance(url) == true){
            Log.d("resim cache: ", "cache'den yükleme");
            return loadImageFromStorage(url);
        }else{
            Log.d("resim cache: ", "internetten yükleme");
            if( url.length() == 0 ) return null;
            HttpGet httpGet = new HttpGet(url);
            DefaultHttpClient client = new DefaultHttpClient();
            HttpResponse response = null;
            try {
                response = client.execute(httpGet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            InputStream is = null;
            try {
                is = new BufferedInputStream( response.getEntity().getContent() );
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                if( preferredWidth > 0 && preferredHeight > 0 && bitmap.getWidth() > preferredWidth && bitmap.getHeight() > preferredHeight ) {
                    bitmap = Bitmap.createScaledBitmap(bitmap, preferredWidth, preferredHeight, false);
                    saveToInternalSorage(bitmap, url);
                    return bitmap;
                } else {
                    saveToInternalSorage(bitmap, url);
                    return bitmap;
                }
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onPostExecute( Bitmap drawable ) {
        imageView.setImageBitmap( drawable );
    }


    public boolean fileExistance(String fname){
        String[] values = fname.split("/");
        fname = values[values.length-1];

        ContextWrapper cw = new ContextWrapper(Anket.getContext().getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);

        File file = new File(directory, fname);
        return file.exists();
    }

    private Bitmap loadImageFromStorage(String path)
    {
        try {
            String[] values = path.split("/");
            path = values[values.length-1];

            ContextWrapper cw = new ContextWrapper(Anket.getContext().getApplicationContext());
            // path to /data/data/yourapp/app_data/imageDir
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            // Create imageDir
            File mypath=new File(directory, path);
            //File f=new File(path);
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(mypath));
            return b;
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private String saveToInternalSorage(Bitmap bitmapImage, String url){
        String[] values = url.split("/");
        url = values[values.length-1];

        ContextWrapper cw = new ContextWrapper(Anket.getContext().getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, url);

        Log.d("cw: " , cw.toString());
        Log.d("directory: ", directory.toString());

        FileOutputStream fos = null;
        try {

            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("getAbsolutePath: " , directory.getAbsolutePath().toString());
        return directory.getAbsolutePath();
    }

}