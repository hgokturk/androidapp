package com.example.deathstar.ankettest;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class trending extends AsyncTask<Void, Void, JSONArray> {

    trending() {

    }

    @Override
    protected JSONArray doInBackground(Void... params) {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://komando.bilgikurumsal.org/anket/index.php");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("action", "trending"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpContext httpContext = new BasicHttpContext();
            HttpResponse response = httpclient.execute(httppost,httpContext);

            HttpEntity resEntity = response.getEntity();
            String data = EntityUtils.toString(resEntity);

            try {
                JSONArray jArray = new JSONArray(data);
                //Log.i("trending", jArray.toString());

                /*JSONObject jObject = new JSONObject(data);
                Log.i("trending", jObject.toString());*/
                return jArray;
                //boolean booleanValue=jObject.getBoolean("isloggedin");

            } catch (JSONException e) {
                Log.d("hata: " , e.getMessage());
                e.printStackTrace();
                return null;
            }


        } catch (ClientProtocolException e) {
            Log.d("hata: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.d("hata: ", e.getMessage());
            return null;
        }
    }

    /*public JSONArray arrVer(JSONArray arr){
        return arr;
    }*/

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        super.onPostExecute(jsonArray);
        /*if(jsonArray != null){
            arrVer(jsonArray);
        }*/
    }

    @Override
    protected void onCancelled() {

    }
}